package BookStore;
import java.util.*;
public class Library{
	private String treemap;
    private ArrayList<Book> bookList=new ArrayList<Book>();
    
    public void setBookList(ArrayList<Book> bookList){
        this.bookList=bookList;
    }
    public ArrayList<Book> getBookList(){
        return bookList;
    }
    public void addBook(Book bobj){
        bookList.add(bobj);
    }
    public boolean isEmpty(){
        return bookList.isEmpty();
    }
    public ArrayList<Book> viewAllBooks(){
        return bookList;
    }
    public ArrayList<Book> viewBooksByName(String Name){
        ArrayList<Book> result=new ArrayList<Book>();
        Iterator iter=bookList.iterator();
        while(iter.hasNext())
        {
            Book b=(Book)iter.next();
            if(b.getGenre().equalsIgnoreCase(Name))
            result.add(b);
        }
        return result;
    }
    public int countnoofbook(String bname){
        int count=0;
        Iterator iter=bookList.iterator();
        while(iter.hasNext())
        {
            Book b=(Book)iter.next();
            if(b.getBookName().equalsIgnoreCase(bname))
            count++;
        }
        return count;
    }
        public int SearchByGenre(String bname){
            int count_genre=0;
            Iterator iter1=bookList.iterator();
            while(iter1.hasNext())
            {
                Book b=(Book)iter1.next();
                if(b.getGenre().equalsIgnoreCase(bname))
                count_genre++;
            }
   
    return count_genre;
        }
		public ArrayList<Book> remove() {
			// TODO Auto-generated method stub
			return null;
		}
		
		
				
			
	
}




